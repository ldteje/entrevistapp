﻿##Proyecto Entrevistapp
El proyecto implementa una web para administrar entrevistas.
## API REST    

### /api/entrevistas

```javascript   
POST /api/entrevistas/		@RequestBody Entrevista entrevista  
								RESPONSE: Entrevista  
```
**Documentación**    

* [Diagrama ER](https://bitbucket.org/somospnt/entrevistapp/src/master/doc/diagrama/)  
* [Diagramas de clases](https://drive.google.com/file/d/1C_XVAB_WjF25ftpyMkz3-ztEO9HZjWeX/view?usp=sharing) 
* [Diagramas de dominio](https://drive.google.com/file/d/1VeGCCDYv_naKPBe3mWsSKJvttF2lWsmK/view?usp=sharing)  
* [DDT](https://bitbucket.org/somospnt/entrevistapp/src/master/doc/ddt/ddt.md)

