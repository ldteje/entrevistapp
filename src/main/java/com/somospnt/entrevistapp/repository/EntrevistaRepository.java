package com.somospnt.entrevistapp.repository;

import com.somospnt.entrevistapp.domain.Entrevista;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntrevistaRepository extends JpaRepository<Entrevista, Long> {

}
