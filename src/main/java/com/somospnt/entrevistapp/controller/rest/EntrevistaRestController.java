package com.somospnt.entrevistapp.controller.rest;

import com.somospnt.entrevistapp.domain.Entrevista;
import com.somospnt.entrevistapp.service.EntrevistaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/entrevistas")
public class EntrevistaRestController {
    @Autowired
    private EntrevistaService entrevistaService;

    @PostMapping("/")
    public Entrevista crear(@RequestBody Entrevista entrevista) {
        return entrevistaService.crear(entrevista);       
    }
}
