package com.somospnt.entrevistapp.controller;

import com.somospnt.entrevistapp.domain.Entrevista;
import com.somospnt.entrevistapp.service.EntrevistaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class EntrevistaController {

    @Autowired
    private EntrevistaService entrevistaService;

    @RequestMapping("/")
    public String entrevistas(Model model) {
       List<Entrevista> entrevistas = entrevistaService.buscarTodos();
        model.addAttribute("entrevistas", entrevistas);
        return "entrevistas";
    }
}
