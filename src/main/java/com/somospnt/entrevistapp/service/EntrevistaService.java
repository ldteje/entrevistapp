package com.somospnt.entrevistapp.service;

import com.somospnt.entrevistapp.domain.Entrevista;
import com.somospnt.entrevistapp.repository.EntrevistaRepository;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Service
@Transactional
public class EntrevistaService {

    private final EntrevistaRepository entrevistaRepository;

    public EntrevistaService(EntrevistaRepository entrevistaRepository) {
        this.entrevistaRepository = entrevistaRepository;
    }

    public List<Entrevista> buscarTodos() {
        return entrevistaRepository.findAll();
    }

    public Entrevista crear(Entrevista entrevista) {
        Assert.notNull(entrevista.getNombre(), "La entrevista debe contener nombre y fecha");
        Assert.notNull(entrevista.getFecha(), "La entrevista debe contener nombre y fecha");
        Assert.isTrue(entrevista.getNombre().length() > 0, "La entrevista debe contener nombre y fecha");
       
        return entrevistaRepository.save(entrevista);
    }

}
