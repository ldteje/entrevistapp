pnt.service.entrevistas = (function () {

    var urlService = "/api/entrevistas/";

    function crear(entrevista) {
        return $.ajax({
            data: JSON.stringify(entrevista),
            type: "POST",
            contentType: "application/json",
            url: urlService
        });
    }

    return {
        crear: crear
    };

})();



