pnt.ui.entrevistas = (function () {

    var $bodyTablaEntrevistas = $("#bodyTablaEntrevistas");
    function init() {
        initSubmitCrear();
    }

    function initSubmitCrear() {
        $(function () {
            $(".pnt-js-form-crear-nombre-input")[0].oninvalid = function () {
                this.setCustomValidity("El nombre debe poseer sólo letras y un máximo de 50 caracteres");
            };
        });
        $(".pnt-js-form-crear").submit(function (event) {
            var entrevista = {
                id: null,
                fecha: $(".pnt-js-form-crear-fecha-input").val(),
                nombre: $(".pnt-js-form-crear-nombre-input").val(),
                comentario: $(".pnt-js-form-crear-comentario-textarea").val()
            };
            crear(entrevista);
            event.preventDefault();
        });
    }
    function crear(entrevista) {
        pnt.service.entrevistas.crear(entrevista)
                .done(mostrar);
    }

    function mostrar(entrevista) {
        var fecha = new Date(entrevista.fecha);
        var options = {year: 'numeric', month: '2-digit', day: '2-digit'};
        $bodyTablaEntrevistas.html($bodyTablaEntrevistas.html() + "<tr><td>" + fecha.toLocaleDateString('es-AR', options) + "</td><td>"
                + entrevista.nombre + "</td><td>" + entrevista.comentario + "</td></tr>");
    }

    return {
        init: init
    };
})();
$(document).ready(function () {
    pnt.ui.entrevistas.init();
});

