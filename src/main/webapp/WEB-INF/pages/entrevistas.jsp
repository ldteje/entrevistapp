<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<html>
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>EntrevistAPP</title>

        <!-- Bootstrap Core CSS -->
        <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/css/entrevista.css" rel="stylesheet" type="text/css"/>
        <link rel="icon" href="/vendor/img/Meeting-512.png">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top " role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <img class="img-responsive2" src="/vendor/img/Meeting-512-white.png"  height="42" width="42">
                    <a class="navbar-brand" href="#">EntrevistAPP</a>
                </div>
            </div>
        </nav>
        <div class="container pnt-contenedor-de-tabla">
            <div class="row">
                <div class="col-12 justify-content-start text-center">
                    <div class="row col-12 mu-4 justify-content-center">
                        <h4>Mis Entrevistas</h4>
                    </div>
                    <form class="pnt-js-form-crear" method="post">

                        <div class="row input-group">
                            <div class="col-2">
                                <input type="date" class="form-control pnt-js-form-crear-fecha-input"placeholder="Fecha" required>
                            </div>
                            <div class="col-2">
                                <input type="text" class="form-control pnt-js-form-crear-nombre-input" placeholder="Nombre" required pattern="[A-Za-z]{1,50}">
                            </div>
                            <div class="col-6">
                                <textarea class="form-control form-control-sm pnt-js-form-crear-comentario-textarea" rows="1" maxlength="1024" placeholder="Comentario"></textarea>
                            </div>
                            <div class="col-1">
                                <button type="submit" class="btn btn-primary btn-lg">Crear</button>
                            </div>
                        </div>
                    </form>


                    <div class="row">
                        <div class="col-12">
                            <table class="table table-bordered">
                                <thead class="thead-light">
                                    <tr>
                                        <th class="col-md-2">D�a</th>
                                        <th class="col-md-2">Nombre</th>
                                        <th class="col-md-8">Comentario</th>
                                    </tr>
                                </thead>
                                <tbody id="bodyTablaEntrevistas">
                                    <c:forEach var="entrevista" items="${entrevistas}">
                                        <tr>
                                            <td><fmt:formatDate pattern = "dd/MM/yyyy"
                                                            value = "${entrevista.fecha}" /></td>
                                            <td>${entrevista.nombre}</td>
                                            <td>${entrevista.comentario}</td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Lib JS -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
        <!-- JS de la aplicacion -->
        <script src="js/app.js"></script>
        <script src="js/service/entrevistas.js"></script>
        <script src="js/ui/views/entrevistas.js"></script>
    </body>
</html>

