DROP TABLE IF EXISTS entrevista;

CREATE TABLE entrevista (
    id BIGINT IDENTITY PRIMARY KEY,
    nombre VARCHAR(50),
    comentario VARCHAR(1024),
    fecha DATE
);


INSERT INTO entrevista VALUES (1, 'Juan', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vel nibh ut diam efficitur sodales. Sed malesuada nisi nibh, et mattis lacus auctor nec. Nunc at metus sit amet leo placerat tempor. Vivamus in semper est. Etiam pulvinar lectus turpis, non scelerisque nisl egestas in. Phasellus lectus arcu, efficitur rutrum lacus non, placerat tincidunt mauris. Nam ac maximus urna. Sed consectetur ullamcorper lectus, pretium vestibulum sapien interdum lobortis. Vestibulum sed elit sed erat tincidunt venenatis in at felis. Nullam varius laoreet congue. Praesent id molestie magna. In non enim nulla. Nullam pulvinar ligula et est scelerisque, sed eleifend nisl suscipit. Vivamus tincidunt nisi vitae odio bibendum efficitur. Fusce aliquet ultrices erat nec porta. Aliquam scelerisque condimentum lectus id consectetur', '2018-04-18');
INSERT INTO entrevista VALUES (2, 'Pedro', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (3, 'Tomas', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (4, 'Juan', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (5, 'Pedro', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (6, 'Tomas', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (7, 'Juan', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (8, 'Pedro', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (9, 'Tomas', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (10, 'Juan', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (11, 'Pedro', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (12, 'Tomas', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (13, 'Juan', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (14, 'Pedro', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (15, 'Tomas', 'descripcion1', '2018-04-18');
