DROP TABLE IF EXISTS entrevista;

CREATE TABLE entrevista (
    id BIGINT IDENTITY PRIMARY KEY,
    nombre VARCHAR(50),
    comentario VARCHAR(50),
    fecha DATE
);


INSERT INTO entrevista VALUES (1, 'Juan', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (2, 'Pedro', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (3, 'Tomas', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (4, 'Juan', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (5, 'Pedro', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (6, 'Tomas', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (7, 'Juan', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (8, 'Pedro', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (9, 'Tomas', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (10, 'Juan', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (11, 'Pedro', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (12, 'Tomas', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (13, 'Juan', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (14, 'Pedro', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (15, 'Tomas', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (16, 'Juan', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (17, 'Pedro', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (18, 'Tomas', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (19, 'Juan', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (20, 'Pedro', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (21, 'Tomas', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (22, 'Juan', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (23, 'Pedro', 'descripcion1', '2018-04-18');
INSERT INTO entrevista VALUES (24, 'Tomas', 'descripcion1', '2018-04-18');
