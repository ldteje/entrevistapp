package com.somospnt.entrevistapp;

import com.somospnt.entrevistapp.domain.Entrevista;
import com.somospnt.entrevistapp.repository.EntrevistaRepository;
import com.somospnt.entrevistapp.service.EntrevistaService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Date;
import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationConfig.class)
@Transactional
public class EntrevistaServiceTest {

    @Autowired
    private EntrevistaService entrevistaService;
    @Autowired
    private EntrevistaRepository entrevistaRepository;

    @Test
    public void buscarTodos_conEntrevistasExistentes_retornaListaDeEntrevistas() {
        List<Entrevista> todasLasEntrevistas = entrevistaService.buscarTodos();

        assertNotNull(todasLasEntrevistas);
        assertEquals(entrevistaRepository.count(), todasLasEntrevistas.size());
    }

    @Test
    public void crearEntrevista_conEntrevistaValida_esCreada() throws ParseException {
        Entrevista entrevista = new Entrevista();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date d = sdf.parse("20/04/2018");
        entrevista.setNombre("Luis");
        entrevista.setComentario("The cat is under the table");
        entrevista.setFecha(d);

        entrevistaService.crear(entrevista);

        assertNotNull(entrevista.getId());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void crearEntrevista_sinNombre_lanzaIllegalArgument() throws ParseException {
        Entrevista entrevista = new Entrevista();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date d = sdf.parse("20/04/2018");
        entrevista.setComentario("The cat is under the table");
        entrevista.setFecha(d);

        entrevistaService.crear(entrevista);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void crearEntrevista_conNombreVacio_lanzaIllegalArgument() throws ParseException {
        Entrevista entrevista = new Entrevista();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date d = sdf.parse("20/04/2018");
        entrevista.setComentario("The cat is under the table");
        entrevista.setFecha(d);
        entrevista.setNombre("");

        entrevistaService.crear(entrevista);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void crearEntrevista_sinFecha_lanzaIllegalArgument() throws ParseException {
        Entrevista entrevista = new Entrevista();
        entrevista.setNombre("Luis");
        entrevista.setComentario("The cat is under the table");

        entrevistaService.crear(entrevista);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void crearEntrevista_sinNombreYFecha_lanzaIllegalArgument() throws ParseException {
        Entrevista entrevista = new Entrevista();
        entrevista.setComentario("The cat is under the table");

        entrevistaService.crear(entrevista);
    }
}
